#include <iostream>
#include <cstdlib>
#include"Point.h"
#include <cmath>
#include <iostream>
using namespace std;


Point* tabObjet[10]; //tableau d'objet
int tabChoix[3];// choix des centres initiaux aleatoirement
Point* tabCentreCluster[3];
Point* tabAux1[10];
Point* tabAux2[10];
Point* tabAux3[10];


/* generation des nombres aleatoires*/
int nbAlea(int min,int max){
int nbgen=rand()%max+min;    
}

int min(int x, int y, int z)
{
	if(x<=y && x<=z)
	{
			return x;}
			
	else if(y<=x && y<= z)
		{
		return y;}
	else
	
	
		{
				return z;}
}


int main(int argc, char** argv) {
	
	printf("\n ***       L'algorithme Kmeans      ***  ");
	printf("\n");
	printf("\n");
	
	//remplissage de tableau tabObjet avec des obejts o� les valeurs d'abscisses et d'ordonnees sont generes aleatoirement
	for (int i=0; i<10;i++)
	{
		tabObjet[i]=new Point(nbAlea(0,10),nbAlea(0,10));
		
	}
	printf(" Coordonnees des points : \n");
	printf("\n");
	for(int i=0;i<10;i++)
	{
		printf("( %d , %d )",tabObjet[i]->getX(),tabObjet[i]->getY());
		printf("\n");
	}
	
	for(int i=0;i<3;i++)
	{
		tabChoix[i]=nbAlea(0,10);
	}
	// choix de centres initiaux
	printf("\n");
	printf(" les centres initiaux :");
	printf("\n");
	printf("\n");
	for(int i=0;i<3;i++)
	{
		tabCentreCluster[i]=new Point(tabObjet[tabChoix[i]]->getX(),tabObjet[tabChoix[i]]->getY())	;
		printf("C%d : ( %d , %d )",i+1,tabCentreCluster[i]->getX(),tabCentreCluster[i]->getY());
		printf("\n");
	}
	
	// initialisation des clusters avec les centres initiaux
	tabAux1[0]=new Point(tabCentreCluster[0]->getX(),tabCentreCluster[0]->getY());
	tabAux2[0]=new Point(tabCentreCluster[1]->getX(),tabCentreCluster[1]->getY());
	tabAux3[0]=new Point(tabCentreCluster[2]->getX(),tabCentreCluster[2]->getY());
	
	int a;
	int b;
	int c;
	
	int j;
	int k;
	int l;
	
	int somme_x_c1=0;
	int somme_y_c1=0;
	int nbc1=0;
	
	int somme_x_c2=0;
	int somme_y_c2=0;
	int nbc2=0;
	
	int somme_x_c3=0;
	int somme_y_c3=0;
	int nbc3=0;
	
	int iter = 0;
	printf("\n");
	printf("\n");
	

	printf("\n Veuillez saisir le nomber d'iterations':   ");
	cin>>iter;
	for(int m=0;m<iter;m++)
	{
		printf("Iteration numero : %d",m+1);
	
			
			j=0;
			l=0;
			k=0;
			printf("\n");
			printf("\n");
			printf(" Calcul de distance entre les centres de clusters et les differents points :");
			printf("\n");
			printf("\n");
					for(int i=0;i<10;i++)
					{
							a=0;
							b=0;
							c=0;
						
							a= std::abs(tabObjet[i]->getX()-tabAux1[0]->getX()) + std::abs(tabObjet[i]->getY()-tabAux1[0]->getY());
							
							
							b= std::abs(tabObjet[i]->getX()-tabAux2[0]->getX()) + std::abs(tabObjet[i]->getY()-tabAux2[0]->getY());
							
							
							c= std::abs(tabObjet[i]->getX()-tabAux3[0]->getX()) + std::abs(tabObjet[i]->getY()-tabAux3[0]->getY());
							
							printf("( %d , %d , %d )          ===>  le minimum entre les trois valeurs est : %d",a,b,c,min(a,b,c));
							printf("\n");
							
							if(a==min(a,b,c))
							{
								j=j+1;
								tabAux1[j]=new Point(tabObjet[i]->getX(),tabObjet[i]->getY());
								
							}
							else if(b==min(a,b,c))
							{
								k=k+1;
								tabAux2[k]=new Point(tabObjet[i]->getX(),tabObjet[i]->getY());
								
							
							}
							else
							{
								l=l+1;
								tabAux3[l]=new Point(tabObjet[i]->getX(),tabObjet[i]->getY());
								
								
							}
					
					}
			printf("\n");
			printf("\n");
			printf(" les clusters  :");
			printf("\n");
			printf("\n");
			
			printf(" les objets de cluster C1 sont  :");
			printf("\n");
			printf("\n");
			for(int i=1;i<10;i++)
			{
				if(tabAux1[i]!=NULL)
				{
					printf("( %d , %d )",tabAux1[i]->getX(),tabAux1[i]->getY());
					printf("\n");
				}
				else
				{
					continue;
				}
			}
			
		
			for(int i=1;i<10;i++)
			{
				if(tabAux1[i]!=NULL)
				{
					somme_x_c1=somme_x_c1+tabAux1[i]->getX();
					somme_y_c1=somme_y_c1+tabAux1[i]->getY();
					nbc1++;
				}
				else
				{
					continue;
				}
			}
			tabAux1[0]->setX(somme_x_c1/nbc1);
			tabAux1[0]->setY(somme_y_c1/nbc1);
			
			printf("\n");
			printf("Mise a jour de C1 : ( %d , %d )",tabAux1[0]->getX(),tabAux1[0]->getY());
			printf("\n");
			for(int i=1;i<10;i++)
			{
				if(tabAux1[i]!=NULL)
				{
					tabAux1[i]=NULL;
				}
				else
				{
					continue;
				}
			}
			
			printf("\n");
			printf("\n");
			printf(" les objets de cluster C2 sont  :");
			printf("\n");
			printf("\n");
			for(int i=1;i<10;i++)
			{
				if(tabAux2[i]!=NULL)
				{
					printf("( %d , %d )",tabAux2[i]->getX(),tabAux2[i]->getY());
					printf("\n");
				}
				else
				{
					continue;
				}
			}
			
			
			for(int i=1;i<10;i++)
			{
				if(tabAux2[i]!=NULL)
				{
					somme_x_c2=somme_x_c2+tabAux2[i]->getX();
					somme_y_c2=somme_y_c2+tabAux2[i]->getY();
					nbc2++;
				}
				else
				{
					continue;
				}
			}
			tabAux2[0]->setX(somme_x_c2/nbc2);
			tabAux2[0]->setY(somme_y_c2/nbc2);
			printf("\n");
			printf("Mise a jour de C2 : ( %d , %d )",tabAux2[0]->getX(),tabAux2[0]->getY());
			printf("\n");
			for(int i=1;i<10;i++)
			{
				if(tabAux2[i]!=NULL)
				{
					tabAux2[i]=NULL;
				}
				else
				{
					continue;
				}
			}
			
			
			
			
			
			
			
			
			
			printf("\n");
			printf("\n");
			printf(" les objets de cluster C3 sont  :");
			printf("\n");
			printf("\n");
			
			for(int i=1;i<10;i++)
			{
				if(tabAux3[i]!=NULL)
				{
					printf("( %d , %d )",tabAux3[i]->getX(),tabAux3[i]->getY());
					printf("\n");
				}
				else
				{
					continue;
				}
			}
			
			
			for(int i=1;i<10;i++)
			{
				if(tabAux3[i]!=NULL)
				{
					somme_x_c3=somme_x_c3+tabAux3[i]->getX();
					somme_y_c3=somme_y_c3+tabAux3[i]->getY();
					nbc3++;
				}
				else
				{
					continue;
				}
			}
			tabAux3[0]->setX(somme_x_c3/nbc3);
			tabAux3[0]->setY(somme_y_c3/nbc3);
			printf("\n");
			printf("Mise a jour de C3 : ( %d , %d )",tabAux3[0]->getX(),tabAux3[0]->getY());
			printf("\n");
			for(int i=1;i<10;i++)
			{
				if(tabAux3[i]!=NULL)
				{
					tabAux3[i]=NULL;
				}
				else
				{
					continue;
				}
			}
			
			
			
	
	
	
	
}
	
	
	
	
	return 0;
}
