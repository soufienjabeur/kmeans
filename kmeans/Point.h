#ifndef POINT_H
#define POINT_H
class Point
{
 public:
 	Point(int,int);
 void setX(int x);
 void setY(int y);
 int getX();
 int getY();
 int distance(const Point &P);
 Point milieu(const Point &P);
 void saisir();
 void afficher();
 private:
 int x,y;
};
#endif
